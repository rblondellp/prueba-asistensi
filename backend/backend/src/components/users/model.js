const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const Schema = mongoose.Schema;

const mySchema = new Schema(
    {
        username: {
            type: String,
            index: { unique: true },
            required: true,
        },
        first_name: {
            type: String,
            required: true,
        },
        last_name: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
        },
        password: {
            type: String,
            required: true,
        },
    },
    { timestamps: true }
);

mySchema.methods.encryptPassword = async password => {
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(password, salt);
};

mySchema.methods.validatePassword = function (password) {
    return bcrypt.compare(password, this.password);
};

mySchema.pre("save", function (next) {
    this.updated_at = Date.now();
    next();
});

const model = mongoose.model("User", mySchema);
module.exports = model;
