const Model = require("./model");

async function addUser(user) {
    const myUser = new Model(user);

    myUser.password = await myUser.encryptPassword(myUser.password);
    return myUser.save();
}

async function getUsers(idUser) {
    let filter = {};
    if (idUser != null) {
        filter = {
            _id: idUser,
        };
    }
    const users = await Model.find(filter);
    return users;
}

async function updateUser(id, updatedUser) {
    const user = await Model.findOne({
        _id: id,
    });

    user.username = updatedUser.username;
    user.first_name = updatedUser.first_name;
    user.last_name = updatedUser.last_name;
    user.email = updatedUser.email;
    user.password = updatedUser.password;
    user.password = await user.encryptPassword(user.password);
    const newUserData = await user.save();
    return newUserData;
}

function removeUser(id) {
    return Model.deleteOne({
        _id: id,
    });
}

async function me(id) {
    const user = await Model.findById(id, { password: 0 });
    return user;
}

async function verifyUser(email, password) {
    const user = await Model.findOne({ email: email });
    if (!user) {
        return Promise.reject("El usuario no existe");
    }
    const validPassword = await user.validatePassword(password);

    if (!validPassword) {
        return Promise.reject("Usuario invalido");
    }

    return user;

    // return user;
}

module.exports = {
    add: addUser,
    list: getUsers,
    update: updateUser,
    remove: removeUser,
    me,
    verifyUser,
};
