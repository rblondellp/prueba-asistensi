const express = require("express");
const response = require("../../network/response");
const controller = require("./controller");
const router = express.Router();

const jwt = require("jsonwebtoken");

router.get("/", (req, res) => {
    const searchUser = req.query.id || null;
    controller
        .getUsers(searchUser)
        .then(usersList => {
            response.success(req, res, usersList, 200);
        })
        .catch(e => {
            response.error(req, res, "Error interno", 500, e);
        });
});

router.post("/", (req, res) => {
    controller
        .addUser(req.body)
        .then(data => {
            response.success(req, res, data, 201);
        })
        .catch(err => {
            response.error(req, res, "Error interno", 500, err);
        });
});

router.patch("/:id", (req, res) => {
    controller
        .updateUser(req.params.id, req.body)
        .then(data => {
            response.success(req, res, data, 200);
        })
        .catch(e => {
            response.error(req, res, "Error interno", 500, e);
        });
});

router.delete("/:id", (req, res) => {
    controller
        .deleteUser(req.params.id)
        .then(() => {
            response.success(
                req,
                res,
                `Usuario ${req.params.id} eliminado.`,
                200
            );
        })
        .catch(e => {
            response.error(req, res, "Error interno", 500, e);
        });
});

router.post("/login", (req, res) => {
    controller
        .login(req.body.email, req.body.password)
        .then(data => {
            const token = jwt.sign({ id: data._id }, "secrettokenkey", {
                expiresIn: "7d",
            });

            response.success(req, res, { data, token }, 200);
        })
        .catch(e => {
            response.error(req, res, "Error", 500, e);
        });
});

router.get("/me", (req, res) => {
    const token = req.headers["x-access-token"];
    if (!token) {
        return res.status(401).json({
            auth: false,
            message: "No token provided",
        });
    }

    const decoded = jwt.verify(token, "secrettokenkey");

    controller
        .me(decoded.id)
        .then(data => {
            if (!data) {
                response.error(req, res, "Not found", 404, data);
            }

            response.success(req, res, data, 200);
        })
        .catch(e => {
            response.error(req, res, "Error interno", 500, e);
        });
});

module.exports = router;
