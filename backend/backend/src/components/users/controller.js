const store = require("./store");

function addUser(newUser) {
    if (
        !newUser.username ||
        !newUser.first_name ||
        !newUser.last_name ||
        !newUser.email ||
        !newUser.password
    ) {
        return Promise.reject("Datos inválidos");
    }

    const user = {
        username: newUser.username,
        first_name: newUser.first_name,
        last_name: newUser.last_name,
        email: newUser.email,
        password: newUser.password,
    };

    return store.add(user);
}

function getUsers(idUser) {
    return new Promise((resolve, reject) => {
        resolve(store.list(idUser));
    });
}

function updateUser(id, updatedUser) {
    return new Promise(async (resolve, reject) => {
        if (
            !id ||
            !updatedUser.username ||
            !updatedUser.first_name ||
            !updatedUser.last_name ||
            !updatedUser.email ||
            !updatedUser.password
        ) {
            return reject("Datos inválidos");
        }

        const result = await store.update(id, updatedUser);
        resolve(result);
    });
}

function deleteUser(id) {
    return new Promise((resolve, reject) => {
        if (!id) {
            return reject("id invalido");
        }

        store
            .remove(id)
            .then(() => {
                resolve();
            })
            .catch(e => {
                reject(e);
            });
    });
}

function me(id) {
    return new Promise((resolve, reject) => {
        resolve(store.me(id));
    });
}

async function login(email, password) {
    return new Promise((resolve, reject) => {
        resolve(store.verifyUser(email, password));
    });
}

module.exports = {
    addUser,
    getUsers,
    updateUser,
    deleteUser,
    me,
    login,
};
