/*
Write a function called removeVowels which accepts a string and returns a new string with all of the vowels (both uppercased and lowercased) removed. Every character in the new string should be lowercased.
Examples:
    removeVowels('Elie') // ('l')
    removeVowels('TIM') // ('tm')
    removeVowels('ZZZZZZ') // ('zzzzzz')
*/

function removeVowels(str){
    var result = str.replace(/[aáAÁeéEÉiíIÍoOóÓuúÚ]/g, '');
    return result.toLowerCase();
}

var string = 'Mi nombre es RaymOnd';

var result = removeVowels(string);

console.log(result);