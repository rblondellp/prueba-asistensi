/*
Write a function called extractValue which accepts an array of objects and a key and returns a new array with the value of each object at the key.
Examples:
    var arr = [{name: 'Elie'}, {name: 'Tim'}, {name: 'Matt'}, {name: 'Colt'}]
    extractValue(arr,'name') // ['Elie', 'Tim', 'Matt', 'Colt']
*/

function extractValue(arr, key){
    arr = arr.filter(element => element.hasOwnProperty(key)).map(map => map[key]);
    return arr;
}

var array = [
    {name: 'Elie'}, 
    {name: 'Tim'}, 
    {name: 'Matt'}, 
    {name: 'Colt'}
];

var result = extractValue(array, 'name');

console.log(result);