/*
Write a function called hasNoDuplicates which accepts an array and returns true if there are no duplicate values (more than one element in the array that has the same value as another). If there are any duplicates, the function should return false.
Examples:
    hasNoDuplicates([1,2,3,2]) // false
    hasNoDuplicates([1,2,3]) // true
*/

function hasNoDuplicates(arr){  
    return !arr.some((val, i) => arr.indexOf(val) !== i);
}

var array = [1,2,3,2];

var result = hasNoDuplicates(array);

console.log(result);