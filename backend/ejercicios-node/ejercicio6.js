/*
Write a function called extractKey which accepts an array of objects and some key and returns a new array with the value of that key in each object.
Examples:
    extractKey([{name: 'Elie'}, {name: 'Tim'}, {name: 'Matt'}, {name: 'Colt'}], 'name') // ['Elie', 'Tim', 'Matt', 'Colt']
*/

function extractKey(arr, key){
    arr = arr.filter(element => element.hasOwnProperty(key)).map(map => map[key]);
    return arr;
}

var array = [
    {name: 'Elie'}, 
    {name: 'Tim'}, 
    {name: 'Matt'}, 
    {name: 'Colt'}
];

var result = extractKey(array, 'name');

console.log(result);